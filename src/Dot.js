var Dot = cc.Sprite.extend({
	ctor : function( layer , score) {
		this._super();
		this.score = score;
		this.initWithFile( 'res/images/putin1.png' );
		this.gameLayer = layer;
		this.distance = Dot.DISTANCE;
		this.direction = 0;
		this.started = false;
		this.isPassRound = false;
	},
	move : function() {
		var pos = this.getPosition();
		var posX = pos.x;
		var posY = pos.y;
		this.direction += Dot.SPEED;
		this.direction = Math.round(this.direction*10)/10;
		var newDegree = this.direction;
		var newPosX = Math.sin( newDegree * Math.PI/180 ) * ( radiusCircle + this.distance ) + screenWidth/2 ;
		var newPosY = Math.cos( newDegree * Math.PI/180 ) * ( radiusCircle + this.distance ) + screenHeight/2 ;
		this.setPosition( cc.p( newPosX , newPosY ) );

	},

	update : function() {
		if(this.started) {
			cc.audioEngine.playMusic( 'res/sound/alarm.mp3' );
			this.move();
			if(this.direction > 350 && !this.isPassRound){
				this.score.updateScore();
				console.log("UUUUUUUUU");
				this.isPassRound = true;
			}
				
			if(this.direction > 360)
				this.isPassRound = false;
			this.direction %= 360;
			//this.gameLayer.score.updateScore();//coppy
			if( this.direction == 0 ) {
				console.log("SCOREEEEEE!!!");
				this.gameLayer.score.updateScore();
			}
		}
	},

	start : function() {
		this.started = true;
	},

	changeLane : function() {
		if( this.distance == Dot.DISTANCE ) 
			this.distance = (-1) * Dot.DISTANCE;
		else 
			this.distance = Dot.DISTANCE;
	},

	getDegree : function() {
		return this.direction;
	}


});

Dot.SPEED = 1.6;
Dot.DISTANCE = 15;

