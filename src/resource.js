var arrayNum = []
for (var i = 0 ; i<=9;i++){
	arrayNum.push("res/images/"+i+".png");
}
var res = {
	background_png : "res/images/aqua-sky.png",
	circle_png : "res/images/circle.png",
	circleCC_png : "res/images/circleCC.png",
	circle_old_png : "res/images/green-circle.png",
	ccc_png : "res/images/ccc.png",
	dot_png : "res/images/dot.png",
	score_png : "res/images/score-circle.png",
	ob1_png : "res/images/ob1.png",
	ob2_png : "res/images/ob2.png",
	line_png : "res/images/line.png",
	color_circle : "res/images/color-circle.png",
	obama_png : "res/images/obama.png",
	dot36_png : "res/images/dot36.png",
	pu1 : "res/images/putin1.png",
	pu2 : "res/images/putin2.png",
	grid : "res/images/grid.png",
	yes_sound : "res/sound/sayyes.mp3",
	end_sound : "res/sound/end.wav",
	alarm_sound : "res/sound/alarm.mp3"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
for (var i in arrayNum){
	g_resources.push(arrayNum[i]);
}