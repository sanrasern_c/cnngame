var score = 0;
var GameLayer = cc.LayerColor.extend({
	init: function() {
		this._super( new cc.Color( 127, 127, 127, 127, 255 ) );
		this.setPosition ( new cc.Point( 0, 0 ) );

		this.background = cc.Sprite.create("res/images/aqua-sky.png");
		this.background.setPosition( cc.p( screenWidth/2 , screenHeight/2 ) );
		this.addChild( this.background , 0 );

		this.score = new Score();
		this.score.setPosition ( cc.p( screenWidth/2 , screenHeight/2 ) ); 
		this.addChild( this.score,4 );
		this.score.scheduleUpdate();

		this.circle = new Circle( this.score);
		this.circle.setPosition ( cc.p ( screenWidth/2 , screenHeight/2 ) );
		this.circle.scheduleUpdate();
		this.addChild( this.circle , 1 );

		this.dot = new Dot( this, this.score );
		this.dot.setPosition ( cc.p( (screenWidth/2)  , 481 ) );
		this.addChild( this.dot , 2 );

		this.text = cc.LabelTTF.create('ENTER to start | SPACE to change !!!' , 'Arial' , 20);
		this.text.setPosition (cc.p( screenWidth/2 , 40));
		this.addChild( this.text , 2);

		// this.c = new C();
		// this.c.setPosition ( cc.p ( screenWidth/2 , screenHeight/2 + 300 ) );
		// this.c.scheduleUpdate();
		// this.addChild( this.c , 1 );


		this.scheduleUpdate();
		this.dot.scheduleUpdate();
		this.addKeyboardHandlers();		


		this.obstracles = [];
		for( var i=0; i<8; i++ ) {
			var obstracle = new Obstracle(i,this.dot,this.circle,this.score,this.gamelayer);
			this.obstracles.push(obstracle);
			this.addChild(obstracle,3);
		}

		this.state = GameLayer.STATES.STOP;

		return true;

	},

	// update : function( dt ) {		
	// },

	addKeyboardHandlers : function() {
		var self = this;
		cc.eventManager.addListener( {
			event : cc.EventListener.KEYBOARD,
			onKeyPressed : function( keyCode , event ) {
				self.onKeyDown( keyCode , event );
			},
			onKeyReleased : function( keyCode , event ) {
				self.onKeyUp( keyCode , event );
			}
		} , this );
	},

	onKeyDown : function( keyCode , event ) {
		console.log ( 'Down : ' + keyCode.toString() );

			if( this.state == GameLayer.STATES.STOP && keyCode == cc.KEY.enter ) {
				this.state = GameLayer.STATES.START;
				this.startGame();
				//this.dot.changeLane();
			}
			else if( keyCode == cc.KEY.space ) {
				this.dot.changeLane();
			}
		
		
	},

	startGame : function() {
		this.dot.start();
		this.dot.changeLane();
		this.circle.start();

	},

	onKeyUp : function( keyCode , event ) {
		console.log( 'Up : ' + keyCode.toString() );
	}
});

GameLayer.STATES = {
	STOP: 0,
	START: 1
};

var StartScene = cc.Scene.extend( {
	onEnter: function() {
		this._super();
		var layer = new GameLayer();
		layer.init();
		this.addChild(layer);
	}
});