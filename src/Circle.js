var Circle = cc.Sprite.extend({
	ctor : function(){
		this._super();
		this.direction = 0;
		this.initWithFile( 'res/images/color-circle.png' );
		this.scheduleUpdate();
		this.started = false;
	},

	update : function(){
		if(this.started) {
			this.move();
		}
	},

	start : function() {
		this.started =true;
	},

	move : function() {
		var pos = this.getPosition();
		var posX = pos.x;
		var posY = pos.y;
		var degree = this.direction;
		this.setRotation( degree + Circle.SPEED );
		var newDegree = this.getRotation();
		this.direction = newDegree;
	}
});

Circle.SPEED = 4;
