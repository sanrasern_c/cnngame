var Obstracle = cc.Sprite.extend({
	ctor : function(index,dot,circle,score,gamelayer) { 
		this._super();
		this.circle = circle;
		//this.score = score;
		this.gamelayer = gamelayer;
		this.score = score;
		this.initWithFile( 'res/images/line.png');
		this.dot = dot;
		this.started = false;
		this.flip = false;
			
			this.degree = index*45;

			this.r = radiusCircle*(Math.random()>0.41 ? 1.06 : 0.93 );

			if (this.degree==0 || this.degree==45) {
				this.r = radiusCircle*(1.06);
			} 

			var posX = (Math.sin(this.degree*Math.PI/180)*this.r) + (screenHeight/2) -100 ;
			var posY = (Math.cos(this.degree*Math.PI/180)*this.r) + (screenWidth/2) +100;
			this.setPosition(cc.p(posX,posY));
			this.setRotation(this.degree);
			this.scheduleUpdate();
	},

	update : function() {
			this.isPass();
			this.isHit();
	},

	start : function() {
		this.started = true;
	},

	isPass : function() {
			//this.score += 3;
			console.log('thois.score = ' + this.score);
			this.degree -= 0.2;
			var posX = 0;
			var posY = 0;
			var r = radiusCircle*(Math.random()>0.5 ? 1.06 : 0.93 );
				var diff = this.dot.getDegree() - ((this.degree)%360);
				if( diff > 5 && diff < 7  ) {
					console.log("XXXXX");
					posX = (Math.sin(this.degree*Math.PI/180)* r) + (screenHeight/2) -100 ;
					posY = (Math.cos(this.degree*Math.PI/180)* r) + (screenWidth/2) +100;
					this.r = r;
				}
				else {
					posX = (Math.sin(this.degree*Math.PI/180)* this.r) + (screenHeight/2) -100 ;
					posY = (Math.cos(this.degree*Math.PI/180)* this.r) + (screenWidth/2) +100;
				}
				this.setPosition(cc.p(posX,posY));
				this.setRotation(this.degree);
			
	},


	isHit : function() {
			var ObPosX = this.getPositionX();
			var ObPosY = this.getPositionY();
			var dotPosX = this.dot.getPositionX();
			var dotPosY = this.dot.getPositionY();
			var distanceX = Math.abs(ObPosX - dotPosX);
			var distanceY = Math.abs(ObPosY - dotPosY);
			if( distanceX <= 6 && distanceY <= 6 ) {
				console.log('HIT');
				this.dot.setPosition( cc.p(0,0) );
				this.dot.unscheduleUpdate();
				this.dot.removeFromParent();
				cc.audioEngine.playEffect( 'res/sound/end.wav' );
				this.unscheduleUpdate();
				cc.director.runScene(new StartScene());
			}
		
	}


});