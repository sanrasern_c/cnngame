var Score = cc.Node.extend({
	ctor: function(){
		this._super();
		this.score = 0;
		this.First = cc.Sprite.create();
		this.First.setPosition(cc.p(-20,0));
		this.addChild(this.First);
		this.Second = cc.Sprite.create();
		this.Second.setPosition(cc.p(30,0));
		this.addChild(this.Second);
		this.Third = cc.Sprite.create();
		this.Third.setPosition(cc.p(70,0));
		this.addChild(this.Third);
		this.First.initWithFile(arrayNum[0]);
		this.Second.initWithFile(arrayNum[0]);
		this.Third.initWithFile(arrayNum[0]);
	},

	update: function(dt) {
	var	firstDigit = parseInt(this.score/100);
	var	secondDigit = parseInt((this.score-(firstDigit*100))/10);
	var	thirdDigit = this.score%10;

		this.First.initWithFile(arrayNum[firstDigit]);
		this.Second.initWithFile(arrayNum[secondDigit]);
		this.Third.initWithFile(arrayNum[thirdDigit]);
	},
	updateScore: function() {
		this.score += 1;
		console.log("newScore = "+this.score);
		cc.audioEngine.playEffect( 'res/sound/sayyes.mp3' );
	}
});